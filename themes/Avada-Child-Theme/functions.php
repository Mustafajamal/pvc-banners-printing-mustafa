<?php

function theme_enqueue_styles() {
		$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ),time());
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles',20);

function avada_lang_setup() {

}
add_action( 'after_setup_theme', 'avada_lang_setup' );

add_filter( 'woocommerce_cart_item_permalink', '__return_null' );

function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){
  wp_safe_redirect( home_url() );
  exit;
}
// define the woocommerce_after_single_product_summary callback 
function action_woocommerce_after_single_product_summary() {  
	
	global $post;
  $terms = get_the_terms( $post->ID, 'product_cat' );
  foreach ($terms  as $term  ) {
      $product_cat_id = $term->term_id;
      $product_cat_name = $term->name;
      break;
  }
  echo $term->name;
  echo $term->name;
  echo $product_cat_name;
  echo "sfsdfsdf";

  if ($product_cat_name == 'Feather Flags') {
        
    return false;
  }
  else if ($product_cat_name == 'table-covers') {
        
    return false;
  }
  else{
    echo do_shortcode('[show_custom_fancy_product_single1]');
  }

}; 
         
// add the action 
add_action( 'woocommerce_before_add_to_cart_form', 'action_woocommerce_after_single_product_summary', 10, 2 );