<?php
/**
 * Header-3-content template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 * @since      5.1.0
 */

if ( 'v4' !== Avada()->settings->get( 'header_layout' ) && Avada()->settings->get( 'header_position' ) == 'Top' ) {
	return;
}

$header_content_3 = Avada()->settings->get( 'header_v4_content' );
?>

<div class="fusion-header-content-3-wrapper">
	<?php if ( 'Tagline' === $header_content_3 ) : ?>
		<h3 class="fusion-header-tagline">
			<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
		</h3>
	<?php elseif ( 'Tagline And Search' == $header_content_3 ) : ?>
		<?php if ( 'Top' === Avada()->settings->get( 'header_position' ) ) : ?>
			<?php if ( 'Right' == Avada()->settings->get( 'logo_alignment' ) ) : ?>
				<h3 class="fusion-header-tagline">
					<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
				</h3>
				<div class="fusion-secondary-menu-search">
					<?php //get_search_form( true ); ?>
					<?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
				</div>
			<?php else : ?>
				<div class="fusion-secondary-menu-search">
					<?php //get_search_form( true ); ?>
					<?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
				</div>
				<h3 class="fusion-header-tagline">
					<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
				</h3>
			<?php endif; ?>
		<?php else : ?>
			<h3 class="fusion-header-tagline">
				<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
			</h3>
			<div class="fusion-secondary-menu-search">
				<?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
			</div>
		<?php endif; ?>
	<?php elseif ( 'Search' === $header_content_3 ) : ?>
		<div class="fusion-secondary-menu-search">
			<?php //get_search_form( true ); ?>
			<?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
		</div>
	<?php elseif ( 'Banner' === $header_content_3 ) : ?>
		<div class="fusion-header-banner">
			<?php echo do_shortcode( Avada()->settings->get( 'header_banner_code' ) ); ?>
		</div>
	<?php endif; ?>
	<div class="header_info_main">		
		<div class="header_call header_col">		
			<div class="icon"><a href="tel:0161-818-6311"><i class="fa fa-phone"></i></a></div>
			<div class="right_text">
				<div class="upper">0161 818 6311</div>
				<div class="lower">Call us between 9am - 11pm</div>
			</div>
		</div>
		<div class="header_mail header_col">		
			<div class="icon"><a href="mailto:info@pvcbannersprinting.co.uk"><i class="fa fa-envelope"></i></a></div>
			<div class="right_text">
				<div class="upper">info@pvcbannersprinting.co.uk</div>
				<div class="lower">Email us any-time</div>
			</div>
		</div>
	</div>
  <div class="cart_info">
  <!--<a href="<?php //echo site_url().'/cart' ?>" title="Cart" class="header-cart-link is-small"><div class="cart-price"><span class="header-cart-title"> <div class="cart-label">My Cart</div> <div class="woocommerce-Price-amount amount"><?php //echo WC()->cart->get_cart_total(); ?></div> </div></span><div class="cart-icon image-icon">
			<strong><?php //echo WC()->cart->get_cart_contents_count(); ?></strong></div>
		 </a>-->
		 <?php
			wp_nav_menu( array(
    'menu'           => 'Cart Menu', // Do not fall back to first non-empty menu.
    'theme_location' => '__no_such_location',
    'fallback_cb'    => false // Do not fall back to wp_page_menu()
) );
		?>
	</div>
</div>
